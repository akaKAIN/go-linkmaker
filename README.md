# go-linkmaker

Небольшой сервис для предоставления пользователю короткой ссылки на желаемый ресурс, с небольшой статистикой.

Структура проекта: **Многослойная**.

 - back repo: https://gitlab.com/akaKAIN/go-linkmaker
 - front repo: https://gitlab.com/akaKAIN/vue-linkmaker
 - front demo: https://get-short-link.web.app/
